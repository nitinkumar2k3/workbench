from workbench.db import db


class UserInfoModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    phonenumber = db.Column(db.String(80))
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))
    USER = ""
    PWD = ""

    def __init__(self, firstname, lastname, phonenumber):
        global USER
        self.firstname = firstname
        self.lastname = lastname
        self.phonenumber = phonenumber
        self.password = "admin"
        self.username = USER

    def json(self):
        return {
            'firstname': self.firstname,
            'lastname': self.lastname,
            'phonenumber': self.phonenumber,
            'username': self.username
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    def update_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        global USER
        USER = username
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()
