from flask_restful import Resource, reqparse
from werkzeug.security import safe_str_cmp

from workbench.models.UserInfoModel import UserInfoModel

_user_parser = reqparse.RequestParser()
_user_parser.add_argument('username',
                          type=str,
                          required=True,
                          help='User Name i.e email id  can not be blank'
                          )

_user_parser.add_argument('password',
                          type=str,
                          required=True,
                          help='Password can not be blank'
                          )


class UserLogin(Resource):
    """
        Use for getting the login user
        with user details
    """

    @classmethod
    def get(cls):
        pass

    """
        Use to login by email and password
     """

    def post(self):
        # get data from parser
        data = _user_parser.parse_args()
        # find user in database
        user = UserInfoModel.find_by_username(data['username'])
        # check password
        if user and safe_str_cmp(user.password, data['password']):
            return user.json(), 201

        return {'message': 'Invalid credential'}, 401
