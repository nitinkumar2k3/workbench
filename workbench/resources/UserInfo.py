from flask_restful import Resource, reqparse

from workbench.models.UserInfoModel import UserInfoModel

_user_parser = reqparse.RequestParser()
_user_parser.add_argument('firstname',
                          type=str,
                          required=True,
                          help='firstname  can not be blank'
                          )
_user_parser.add_argument('lastname',
                          type=str,
                          required=True,
                          help='lastname  can not be blank'
                          )
_user_parser.add_argument('phonenumber',
                          type=str,
                          required=True,
                          help='phonenumber  can not be blank'
                          )
# _user_parser.add_argument('username',
#                           type=str,
#                           required=True,
#                           help='username  can not be blank'
#                           )
class UserInfo(Resource):

    def get(self, name):
        userInfoItem = UserInfoModel.find_by_username(name)
        if userInfoItem:
            return userInfoItem.json()
        return {'message': 'Item not found'}, 404

    def post(self, name):
        data = _user_parser.parse_args()
        # data = request.args()
        print("====================", data.keys())
        item = UserInfoModel.find_by_username(name)
        if item:
            item = UserInfoModel(**data)
            return {'message': 'User exist with the same email id'}, 404
        else:
            # item = UserInfoModel(firstname=data['firstname'], lastname=data['lastname'],phonenumber=data['phonenumber'])
            item = UserInfoModel(**data)
            item.save_to_db()
            return item.json()

    def put(self, name):
        data = _user_parser.parse_args()
        print("====================", data.keys())
        item = UserInfoModel.find_by_username(name)

        if item:
            item = UserInfoModel(**data)
            item.phonenumber = data['phonenumber']
            item.firstname = data['firstname']
            item.lastname = data['lastname']
            # item.username = data['username']
            item.update_db()
            item = UserInfoModel.find_by_username(name)
            item.delete_from_db()
            return {'message': 'User details are updated'}, 200
        else:
            return {'message': 'User detail not found'}, 404
