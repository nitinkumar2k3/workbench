from flask_restful import Resource, reqparse

from workbench.models.UserModel import UserModel

_user_parser = reqparse.RequestParser()
_user_parser.add_argument('password',
                          type=str,
                          required=True,
                          help='Password can not be blank'
                          )


class CreatePassword(Resource):

    def post(self, name):
        data = _user_parser.parse_args()
        if UserModel.find_by_username(name):
            return {"message": "User with that username already exists."}, 400
        user = UserModel(username=name, password=data['password'])
        # HOw to do with  unpacking of data **data
        # user = UserModel(**data)
        user.save_to_db()
        return user.json(), 201
