import sqlite3

con = sqlite3.connect("data.db")
cursor = con.cursor()

create_table = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username text, password text)"
cursor.execute(create_table)
insert_query = "insert into users values(?,?,?)"

user = [
    (1, 'super', 'super'),
    (2, 'admin', 'admin')
]
cursor.executemany(insert_query, user)
con.commit()
con.close()
