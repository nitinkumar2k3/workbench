from flask import Flask
from flask_restful import Api

from workbench.resources import UserLogin
from workbench.resources.CreatePassword import CreatePassword
from workbench.resources.UserInfo import UserInfo
from workbench.resources.UserLogin import UserLogin
from workbench.resources.UserRegister import UserRegister

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
# tell not to use flask sql track update alchamy
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#Auto incriment SQLAlchemy
app.config['SQLALCHEMY_ECHO'] = True
api = Api(app)


@app.before_first_request
def create_tables():
    db.create_all()


# api.add_resource(User, '/user/<int:user_id>')
api.add_resource(UserRegister, '/register')
api.add_resource(CreatePassword, '/create/<string:name>')
api.add_resource(UserInfo, '/userinfo/<string:name>')
api.add_resource(UserLogin, '/login')

if __name__ == '__main__':
    from workbench.db import db
    db.init_app(app)
    app.run(port=5000, debug='true')
